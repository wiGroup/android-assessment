# README #


### What is this repository for? ###

* This repo is for an assessment of a developers Android skills

### How do I get set up? ###

* Install Android Studio
* Install JDK
* Install GenyMotion

### TODO v.1.0.0: ###

* Create an Android application that has 3 swipe through walkthrough views
* Create a login screen, that presents itself after swiping through the walkthrough screens
* Store the password & username in the sharedsettings
* Create a tableLayout which will consume this feed http://feeds.news24.com/articles/Fin24/Tech/rss and display the title, description and an image if there is one.
* On correct entry of the password and username, present the tableLayout to the user
* Create a detail view, that once the user clicks on a row, the detail view will display the whole article.

### Who do I talk to? ###

* Mandi van der Merwe